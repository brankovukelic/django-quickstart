"""
Django settings for {{ project_name }} project.
"""

from os.path import abspath, basename, dirname, join


DEBUG = True
TEMPLATE_DEBUG = DEBUG
PROJECT_NAME = basename(dirname(__file__))
PROJECT_ROOT = abspath(dirname(dirname(__file__)))


def in_root(path):
    return abspath(join(PROJECT_ROOT, path))


##############################################################################
# Admins
##############################################################################

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS


##############################################################################
# Security
##############################################################################

ALLOWED_HOSTS = []
SECRET_KEY = '{{ secret_key }}'
SITE_ID = 1

TIME_ZONE = 'America/Chicago'
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True
USE_TZ = True


##############################################################################
# Media and static
##############################################################################

MEDIA_ROOT = '/srv/uploads/%s' % PROJECT_NAME
MEDIA_URL = '/media/'
STATIC_ROOT = '/srv/static/%s' % PROJECT_NAME
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    in_root('static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


##############################################################################
# Application basics
##############################################################################

ROOT_URLCONF = '{{ project_name }}.urls'
WSGI_APPLICATION = '{{ project_name }}.wsgi.application'


##############################################################################
# Templates
##############################################################################

TEMPLATE_DIRS = (
    in_root('templates'),
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    "django.core.context_processors.i18n",
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.debug',
    'django.core.context_processors.tz',
    'django.core.context_processors.csrf',
    'django.core.context_processors.request',
    #'ga_tracking.context_processors.ga_tracking_id',
)


##############################################################################
# Databases
##############################################################################

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '%s' % PROJECT_NAME,
        'USER': 'postgres',
    }
}


##############################################################################
# Middleware and apps
##############################################################################

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'grappelli.dashboard',
    'grappelli',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'south',
    #'ga_tracking',
    #'robots',
    #'ckeditor',
)


##############################################################################
# Middleware and apps
##############################################################################

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


##############################################################################
# Caching framework
##############################################################################

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}


##############################################################################
# Email settings
##############################################################################

# EMAIL_HOST = 'smtp.mandrillapp.com'
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True
# EMAIL_HOST_USER = ''
# EMAIL_HOST_PASSWORD = ''
# DEFAULT_FROM_EMAIL = 'root@example.com'
# SERVER_EMAIL = 'root@example.com'
# SEND_BROKEN_LINK_EMAILS = True


##############################################################################
# Apps configurations
##############################################################################

GRAPPELLI_ADMIN_TITLE = PROJECT_NAME
GRAPPELLI_INDEX_DASHBOARD = '%s.dashboard.CustomIndexDashboard' % PROJECT_NAME

# CKEDITOR_UPLOAD_PATH = abspath(join(MEDIA_ROOT, 'editor'))