from __future__ import unicode_literals


def format_phone(s):
    phone_digits = [c for c in s if c.isdigit()]
    return '%s-%s-%s' % (
        ''.join(phone_digits[0:3]),
        ''.join(phone_digits[3:6]),
        ''.join(phone_digits[6:]),
    )