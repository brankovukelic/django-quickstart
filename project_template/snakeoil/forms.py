from __future__ import unicode_literals

from django.forms import widgets
from django.template.defaultfilters import safe


class SnakeFormMixin(object):
    render_labels = True
    add_placeholder = True
    wrap_in_paragraph = True
    label_as_button = True

    def _is_boolean(self, widget):
        return isinstance(widget, widgets.CheckboxInput) \
            or isinstance(widget, widgets.RadioInput)

    def as_snake(self):
        form_html = ''

        if self.non_field_errors():
            form_html += '<ul class="errors form-errors>'
            for error in self.non_field_errors:
                form_html += '<li class="error">%s</li>' % error
            form_html += '</ul>'

        for field in self.hidden_fields():
            form_html += unicode(field)

        for field in self.visible_fields():
            # Add opening DIV
            form_html += '<div class="field%s%s" id="field_%s">' % (
                field.errors and ' errors' or '',
                field.field.required and ' required' or '',
                field.name
            )

            # Open paragraph tag
            form_html += self.wrap_in_paragraph and '<p class="field">' or ''

            # Treat boolean fields differently
            if self._is_boolean(field.field.widget):
                form_html += '<label for="%s" class="%s">%s %s</label>' % (
                    field.auto_id,
                    self.label_as_button and 'button' or 'boolean',
                    field.as_widget(),
                    unicode(field.label),
                )
            else:
                # Render the label
                if self.render_labels:
                    form_html += '<label for="%s">%s:</label> ' % (
                        field.auto_id,
                        field.label
                    )

                # Render the field
                if self.add_placeholder:
                    form_html += field.as_widget(attrs={
                        'placeholder': field.label
                    })
                else:
                    form_html += field.as_widget()

            # Close paragraph tag if needed
            form_html += self.wrap_in_paragraph and '</p>' or ''

            # Render errors
            if field.errors:
                form_html += '<ul class="errors">'
                for error in field.errors:
                    form_html += '<li class="error">%s</li>' % error
                form_html += '</ul>'
            elif field.help_text:
                form_html += self.wrap_in_paragraph and '<p class="help">' or ''
                form_html += unicode(field.help_text)
                form_html += self.wrap_in_paragraph and '</p>' or ''

            # Close the DIV
            form_html += '</div>'

        return safe(form_html)






