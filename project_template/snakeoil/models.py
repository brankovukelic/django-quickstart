from __future__ import unicode_literals

import hashlib

from django.contrib.auth.models import (
    BaseUserManager, PermissionsMixin, AbstractBaseUser
)
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Email is required')

        user = self.model(
            email=UserManager.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.model(
            email=email,
        )

        user.set_password(password)

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.is_verified = True

        user.save(using=self._db)
        return user


class User(PermissionsMixin, AbstractBaseUser):
    """ To Use this user model, add the following bit to settings module:

        AUTH_USER_MODEL = 'snakeoil.User'

    """

    email = models.EmailField(max_length=200, unique=True, db_index=True)
    md5 = models.CharField(max_length=32, editable=False)

    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    is_reliable = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def get_digest(self, s):
        md5 = hashlib.md5()
        md5.update(s)
        return md5.hexdigest()

    def save(self, *args, **kwargs):
        self.md5 = self.get_digest(self.email)
        super (User, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.email