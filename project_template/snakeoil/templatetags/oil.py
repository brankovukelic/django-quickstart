from django import template
from django.template.defaultfilters import safe

from url_tools.templatetags.urls import add_params


register = template.Library()


def pager_page(page_num, current, url, label=None, cls='number'):
    current = page_num == current
    if current:
        return '<li class="current"><span>%s</span></li>' % (
            page_num
        )
    return '<li%s><a href="%s">%s</a></li>' % (
        cls and ' class="%s"' % cls or '',
        add_params(url, page=page_num),
        label or page_num
    )


def icon(icon_name):
    return '<span class="icon-%s"></span>' % icon_name


@register.filter
def pager(page_obj, url):
    total_pages = page_obj.paginator.num_pages
    pager = '<ul class="pager'
    if total_pages > 5:
        pager += ' full'
    pager += '">'

    if not page_obj.has_other_pages():
        return ''

    if page_obj.has_previous() and total_pages > 5:
        pager += pager_page(1, None, url, icon('fast-backward'), 'paging')
        pager += pager_page(page_obj.previous_page_number(), None, url,
                            icon('step-backward'), 'paging')

    start_page = page_obj.number - 2
    pages = 5

    if total_pages <= 5:
        # 1 [2] 3
        start_page = 1
        pages = total_pages
    elif page_obj.number < 3:
        # 1 [2] 3 4 5
        start_page = 1
    elif page_obj.number > total_pages - 2:
        # 3 4 5 [6] 7
        start_page = total_pages - 4

    for p in range(pages):
        pager += pager_page(p + start_page, page_obj.number, url)

    if page_obj.has_next() and total_pages > 5:
        pager += pager_page(page_obj.next_page_number(), None, url,
                            icon('step-forward'), 'paging')
        pager += pager_page(total_pages, None, url, icon('fast-forward'),
                            'paging')

    pager += '</ul>'
    return safe(pager)


